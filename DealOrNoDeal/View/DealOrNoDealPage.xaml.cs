﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using Windows.ApplicationModel.Core;
using Windows.Foundation;
using Windows.UI;
using Windows.UI.ViewManagement;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Media;
using DealOrNoDeal.Model;

// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace DealOrNoDeal.View
{
    /// <summary>
    ///     An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class DealOrNoDealPage
    {
        #region Data members

        /// <summary>
        ///     The application height
        /// </summary>
        public const int ApplicationHeight = 500;

        /// <summary>
        ///     The application width
        /// </summary>
        public const int ApplicationWidth = 500;

        private IList<Button> briefcaseButtons;
        private IList<Border> dollarAmountLabels;

        private readonly GameManager gameManager = new GameManager();

        private bool firstBriefcaseClicked;

        private Button firstBriefcaseButton;

        #endregion

        #region Properties

        /// <summary>
        ///     The player case amount
        /// </summary>
        private object PlayerCase { get; set; }

        /// <summary>
        ///     Gets or sets the banker offer.
        /// </summary>
        /// <value>
        ///     The banker offer.
        /// </value>
        private double BankerOffer { get; set; }

        #endregion

        #region Constructors

        /// <summary>
        ///     Initializes a new instance of the <see cref="DealOrNoDealPage" /> class.
        ///     Post Condition: PlayerCase == 0; BankerOffer == 0;
        /// </summary>
        public DealOrNoDealPage()
        {
            this.InitializeComponent();
            this.initializeUiDataAndControls();
            this.PlayerCase = 0;
            this.BankerOffer = 0;
            this.dealAndNoDealDisabled();
            this.updateGameVersion();
        }

        #endregion

        #region Methods

        private void initializeUiDataAndControls()
        {
            setPageSize();

            this.briefcaseButtons = new List<Button>();
            this.dollarAmountLabels = new List<Border>();
            this.buildBriefcaseButtonCollection();
            this.buildDollarAmountLabelCollection();
        }

        private void updateGameVersion()
        {
            if (this.gameManager.GameVersion == "Mega")
            {
                this.roundLabel.Text = "Welcome to Mega Deal or No Deal!";
                this.updateLabels();
            }
            else if (this.gameManager.GameVersion == "Syndicated")
            {
                this.roundLabel.FontSize = 15;
                this.roundLabel.Text = "Welcome to Syndicated Deal or No Deal!";
                this.updateLabels();
            }
        }

        private void updateLabels()
        {
            var amounts = this.gameManager.Regular;
            var counter = 0;
            foreach (var label in this.dollarAmountLabels)
            {
                if (label.Child is TextBlock dollarTextBlock)
                {
                    dollarTextBlock.Text = amounts.ElementAt(counter).ToString("c0");
                }

                counter++;
            }
        }

        private static void setPageSize()
        {
            ApplicationView.PreferredLaunchViewSize = new Size {Width = ApplicationWidth, Height = ApplicationHeight};
            ApplicationView.PreferredLaunchWindowingMode = ApplicationViewWindowingMode.PreferredLaunchViewSize;
            ApplicationView.GetForCurrentView().SetPreferredMinSize(new Size(ApplicationWidth, ApplicationHeight));
        }

        private void buildDollarAmountLabelCollection()
        {
            this.dollarAmountLabels.Clear();

            this.dollarAmountLabels.Add(this.label0Border);
            this.dollarAmountLabels.Add(this.label1Border);
            this.dollarAmountLabels.Add(this.label2Border);
            this.dollarAmountLabels.Add(this.label3Border);
            this.dollarAmountLabels.Add(this.label4Border);
            this.dollarAmountLabels.Add(this.label5Border);
            this.dollarAmountLabels.Add(this.label6Border);
            this.dollarAmountLabels.Add(this.label7Border);
            this.dollarAmountLabels.Add(this.label8Border);
            this.dollarAmountLabels.Add(this.label9Border);
            this.dollarAmountLabels.Add(this.label10Border);
            this.dollarAmountLabels.Add(this.label11Border);
            this.dollarAmountLabels.Add(this.label12Border);
            this.dollarAmountLabels.Add(this.label13Border);
            this.dollarAmountLabels.Add(this.label14Border);
            this.dollarAmountLabels.Add(this.label15Border);
            this.dollarAmountLabels.Add(this.label16Border);
            this.dollarAmountLabels.Add(this.label17Border);
            this.dollarAmountLabels.Add(this.label18Border);
            this.dollarAmountLabels.Add(this.label19Border);
            this.dollarAmountLabels.Add(this.label20Border);
            this.dollarAmountLabels.Add(this.label21Border);
            this.dollarAmountLabels.Add(this.label22Border);
            this.dollarAmountLabels.Add(this.label23Border);
            this.dollarAmountLabels.Add(this.label24Border);
            this.dollarAmountLabels.Add(this.label25Border);
        }

        private void buildBriefcaseButtonCollection()
        {
            this.briefcaseButtons.Clear();

            this.briefcaseButtons.Add(this.case0);
            this.briefcaseButtons.Add(this.case1);
            this.briefcaseButtons.Add(this.case2);
            this.briefcaseButtons.Add(this.case3);
            this.briefcaseButtons.Add(this.case4);
            this.briefcaseButtons.Add(this.case5);
            this.briefcaseButtons.Add(this.case6);
            this.briefcaseButtons.Add(this.case7);
            this.briefcaseButtons.Add(this.case8);
            this.briefcaseButtons.Add(this.case9);
            this.briefcaseButtons.Add(this.case10);
            this.briefcaseButtons.Add(this.case11);
            this.briefcaseButtons.Add(this.case12);
            this.briefcaseButtons.Add(this.case13);
            this.briefcaseButtons.Add(this.case14);
            this.briefcaseButtons.Add(this.case15);
            this.briefcaseButtons.Add(this.case16);
            this.briefcaseButtons.Add(this.case17);
            this.briefcaseButtons.Add(this.case18);
            this.briefcaseButtons.Add(this.case19);
            this.briefcaseButtons.Add(this.case20);
            this.briefcaseButtons.Add(this.case21);
            this.briefcaseButtons.Add(this.case22);
            this.briefcaseButtons.Add(this.case23);
            this.briefcaseButtons.Add(this.case24);
            this.briefcaseButtons.Add(this.case25);

            this.storeBriefCaseIndexInControlsTagProperty();
        }

        private void storeBriefCaseIndexInControlsTagProperty()
        {
            for (var i = 0; i < this.briefcaseButtons.Count; i++)
            {
                this.briefcaseButtons[i].Tag = i;
            }
        }

        private void briefcase_Click(object sender, RoutedEventArgs e)
        {
            var briefcaseClicked = (Button) sender;

            var idToRemove = this.RetrieveBriefcaseId(briefcaseClicked);

            briefcaseClicked.IsEnabled = false;
            briefcaseClicked.Visibility = Visibility.Collapsed;

            if (this.firstBriefcaseClicked == false)
            {
                this.firstBriefcaseButton = briefcaseClicked;
                this.firstBriefcaseClicked = true;

                this.storePlayerCase(briefcaseClicked);
                this.gameManager.UpdateRoundInformation();
                this.updateCurrentRoundInformation();
            }
            else if (this.gameManager.CurrentRound == this.gameManager.FinalRound)
            {
                this.summaryOutput.Text = "Congrats you win " +
                                          this.gameManager.RetrieveBriefcaseAmountFromId(
                                              this.RetrieveBriefcaseId(briefcaseClicked)).ToString("c2") +
                                          "\n GAME OVER";
                this.disableBriefcaseButtons();
                this.displayGameOverDialog();
            }
            else
            {
                this.revealOpenedCase(idToRemove);
                this.updateCurrentRoundInformation();
            }
        }

        private async void displayGameOverDialog()
        {
            var gameOverDialog = new ContentDialog {
                Title = "Game Over!",
                Content = "Thank you for playing Deal or No Deal. \n Would you like to restart or exit?",
                PrimaryButtonText = "Restart",
                CloseButtonText = "Exit"
            };

            var result = await gameOverDialog.ShowAsync();
            if (result == ContentDialogResult.Primary)
            {
                await CoreApplication.RequestRestartAsync("");
            }
            else
            {
                Application.Current.Exit();
            }
        }

        private void storePlayerCase(Button selectedCase)
        {
            this.gameManager.CaseSelected =
                this.gameManager.RetrieveBriefcaseAmountFromId(this.RetrieveBriefcaseId(selectedCase));
            this.PlayerCase = selectedCase.Content;

            this.summaryOutput.Text = "Your case " + this.PlayerCase;
        }

        private void revealOpenedCase(int idToRemove)
        {
            this.findAndGrayOutGameDollarLabel(this.gameManager.RemoveBriefcaseFromPlay(idToRemove));
            this.gameManager.UpdateCasesLeftToOpenLabel();
        }

        private void findAndGrayOutGameDollarLabel(int amount)
        {
            foreach (var currentDollarAmountLabel in this.dollarAmountLabels)
            {
                if (grayOutLabelIfMatchesDollarAmount(amount, currentDollarAmountLabel))
                {
                    break;
                }
            }
        }

        private static bool grayOutLabelIfMatchesDollarAmount(int amount, Border currentDollarAmountLabel)
        {
            var matched = false;

            if (currentDollarAmountLabel.Child is TextBlock dollarTextBlock)
            {
                var labelAmount = int.Parse(dollarTextBlock.Text, NumberStyles.Currency);
                if (labelAmount == amount)
                {
                    currentDollarAmountLabel.Background = new SolidColorBrush(Colors.Gray);
                    matched = true;
                }
            }

            return matched;
        }

        private int RetrieveBriefcaseId(Button selectedBriefCase)
        {
            var idValue = Convert.ToInt32(selectedBriefCase.Tag);
            return idValue;
        }

        private void updateCurrentRoundInformation()
        {
            var currentRound = this.gameManager.CurrentRound.ToString();
            var numberOfCasesToOpen = this.gameManager.CasesToOpenThisRound;
            var casesToOpen = this.gameManager.CasesToOpen;

            this.roundLabel.Text = "Round " + currentRound + ": " + this.updateCasesLeftToOpen(numberOfCasesToOpen);
            this.casesToOpenLabel.Text = this.updateCasesLeftToOpen(casesToOpen);

            this.retrieveEndOfRoundOffer();
        }

        private string updateCasesLeftToOpen(int numberOfCases)
        {
            if (numberOfCases == 1)
            {
                return numberOfCases + " case to open";
            }

            return numberOfCases + " cases to open";
        }

        private void retrieveEndOfRoundOffer()
        {
            const int noMoreCasesToOpen = 0;

            if (this.gameManager.CasesToOpen == noMoreCasesToOpen)
            {
                var offer = this.gameManager.CurrentOffer;
                this.BankerOffer = offer;

                this.dealAndNoDealEnabled();
                this.updateSummaryOutput();
                this.disableBriefcaseButtons();
            }
        }

        private void dealButton_Click(object sender, RoutedEventArgs e)
        {
            this.dealAndNoDealDisabled();
            this.summaryOutput.Text = "Your case contained " + this.gameManager.CaseSelected.ToString("c2") +
                                      "\n Accepted offer: " + this.gameManager.CurrentOffer.ToString("c2") +
                                      "\n GAME OVER";
            this.displayGameOverDialog();
        }

        private void noDealButton_Click(object sender, RoutedEventArgs e)
        {
            var penultimate = this.gameManager.FinalRound - 1;
            if (this.gameManager.CurrentRound == penultimate)
            {
                this.moveToFinalRound();
            }
            else
            {
                this.moveToNextRound();
                this.updateSummaryOutput();
            }
        }

        private void moveToFinalRound()
        {
            this.gameManager.UpdateRoundInformation();
            this.updateSummaryOutput();
            this.enableBriefcaseButtons();
            this.firstBriefcaseButton.Visibility = Visibility.Visible;

            foreach (var button in this.briefcaseButtons)
            {
                var parent = VisualTreeHelper.GetParent(button) as StackPanel;
                parent?.Children.Remove(button);
                this.panel2.Children.Add(button);
            }
        }

        private void moveToNextRound()
        {
            this.dealAndNoDealDisabled();
            this.gameManager.UpdateRoundInformation();

            this.updateCurrentRoundInformation();
            this.enableBriefcaseButtons();
        }

        private void updateSummaryOutput()
        {
            var minOffer = this.gameManager.MinOffer.ToString("c2");
            var maxOffer = this.gameManager.MaxOffer.ToString("c2");
            var averageOffer = this.gameManager.AverageOffer.ToString("c2");

            if (this.gameManager.CurrentRound == this.gameManager.FinalRound)
            {
                this.roundLabel.Text = "This is the final Round ";
                this.casesToOpenLabel.Text = "Select a case.";

                this.summaryOutput.Text =
                    "Offers: Max: " + maxOffer + "; min: " + minOffer + "; \n" + "          " + " Average: " +
                    averageOffer;

                this.dealAndNoDealDisabled();
            }
            else if (this.gameManager.CasesToOpen != 0)
            {
                this.summaryOutput.Text = "Offers: Max: " + maxOffer + "; Min: " + minOffer + "; \n" +
                                          "           " + "Average: " + averageOffer;
            }
            else
            {
                this.summaryOutput.Text = "Offers: Max: " + maxOffer + "; Min: " + minOffer + "; \n" + "          " +
                                          " Average: " +
                                          averageOffer + "\n" + "Current offer: " + this.BankerOffer.ToString("c2");
            }
        }

        private void dealAndNoDealDisabled()
        {
            this.dealButton.Visibility = Visibility.Collapsed;
            this.dealButton.IsEnabled = false;

            this.noDealButton.Visibility = Visibility.Collapsed;
            this.noDealButton.IsEnabled = false;
        }

        private void dealAndNoDealEnabled()
        {
            this.dealButton.Visibility = Visibility.Visible;
            this.dealButton.IsEnabled = true;

            this.noDealButton.Visibility = Visibility.Visible;
            this.noDealButton.IsEnabled = true;
        }

        private void disableBriefcaseButtons()
        {
            foreach (var button in this.briefcaseButtons)
            {
                button.IsEnabled = false;
            }
        }

        private void enableBriefcaseButtons()
        {
            foreach (var button in this.briefcaseButtons)
            {
                button.IsEnabled = true;
            }
        }

        #endregion
    }
}