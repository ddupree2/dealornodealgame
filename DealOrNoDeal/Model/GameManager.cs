﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace DealOrNoDeal.Model
{
    /// <summary>
    ///     Handles the management of the actual game play.
    /// </summary>
    public class GameManager
    {
        #region Data members

        /// <summary>
        ///     The syndicated version dollar values
        /// </summary>
        public readonly IList<int> Syndicated = new List<int> {
            0, 1, 5, 10, 25, 50, 75, 100, 200, 300, 400, 500, 750, 1000, 2500, 5000, 10000, 25000, 50000, 75000,
            100000, 150000, 200000, 250000, 350000, 500000
        };

        /// <summary>
        ///     The regular version dollar values
        /// </summary>
        public readonly IList<int> Regular = new List<int> {
            0, 1, 5, 10, 25, 50, 75, 100, 200, 300, 400, 500, 750, 1000, 5000, 10000, 25000, 50000, 75000, 100000,
            200000, 300000, 400000, 500000, 750000, 1000000
        };

        /// <summary>
        ///     The mega version dollar values
        /// </summary>
        public readonly IList<int> Mega = new List<int> {
            0, 100, 500, 1000, 2500, 5000, 7500, 10000, 20000, 30000, 40000, 50000, 75000, 100000, 225000, 400000,
            500000, 750000, 1000000, 2000000, 3000000, 4000000, 5000000, 6000000, 8500000, 10000000
        };

        private readonly IList<Briefcase> briefcases = new List<Briefcase>();

        private IList<int> randomAmounts = new List<int>();

        #endregion

        #region Properties

        /// <summary>
        ///     Gets or sets the case selected.
        /// </summary>
        /// <value>
        ///     The case selected.
        /// </value>
        public int CaseSelected { get; set; }

        private RoundManager RoundManager { get; }

        private Banker Banker { get; }

        /// <summary>
        ///     Gets the game version.
        /// </summary>
        /// <value>
        ///     The game version.
        /// </value>
        public string GameVersion { get; }

        /// <summary>
        ///     Gets the current offer.
        /// </summary>
        /// <value>
        ///     The current offer.
        /// </value>
        public double CurrentOffer
        {
            get
            {
                var offer = this.Banker.RetrieveOffer(this.briefcases);

                return offer;
            }
        }

        /// <summary>
        ///     Gets the minimum offer.
        /// </summary>
        /// <value>
        ///     The minimum offer.
        /// </value>
        public double MinOffer
        {
            get
            {
                var offer = this.Banker.MinOffer;
                return offer;
            }
        }

        /// <summary>
        ///     Gets the maximum offer.
        /// </summary>
        /// <value>
        ///     The maximum offer.
        /// </value>
        public double MaxOffer
        {
            get
            {
                var offer = this.Banker.MaxOffer;
                return offer;
            }
        }

        /// <summary>
        ///     Gets the average offer.
        /// </summary>
        /// <value>
        ///     The average offer.
        /// </value>
        public double AverageOffer
        {
            get
            {
                var offer = this.Banker.AverageOffer;
                return offer;
            }
        }

        /// <summary>
        ///     Gets the current round.
        /// </summary>
        /// <value>
        ///     The current round.
        /// </value>
        public int CurrentRound
        {
            get
            {
                var round = this.RoundManager.CurrentRound;
                return round;
            }
        }

        /// <summary>
        ///     Gets the cases to open this round.
        /// </summary>
        /// <value>
        ///     The cases to open this round.
        /// </value>
        public int CasesToOpenThisRound
        {
            get
            {
                var casesToOpenThisRound = this.RoundManager.RetrieveNumberOfCasesToOpen();
                return casesToOpenThisRound;
            }
        }

        /// <summary>
        ///     Gets the cases to open.
        /// </summary>
        /// <value>
        ///     The cases to open.
        /// </value>
        public int CasesToOpen
        {
            get
            {
                var casesToOpen = this.RoundManager.CasesToOpenThisRound;
                return casesToOpen;
            }
        }

        /// <summary>
        ///     Gets the final round.
        /// </summary>
        /// <value>
        ///     The final round.
        /// </value>
        public int FinalRound
        {
            get
            {
                var finalRound = this.RoundManager.FinalRound;
                return finalRound;
            }
        }

        #endregion

        #region Constructors

        /// <summary>
        ///     Initializes a new instance of the <see cref="GameManager" /> class.
        ///     Post condition: CasesSelected == -1; Briefcases populated with twenty-six cases; Game version is checked and
        ///     updated.
        /// </summary>
        public GameManager()
        {
            this.RoundManager = new RoundManager(0, 0);
            this.CaseSelected = -1;
            this.Banker = new Banker(0, this);
            this.GameVersion = "Regular";
            this.checkGameVersion();
            this.RandomizeBriefcaseAmounts();
        }

        #endregion

        #region Methods

        /// <summary>
        ///     Gets the briefcase amount from identifier.
        ///     Precondition: A briefcase is selected.
        ///     Post condition: value = dollar amount associated with that case.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>
        ///     Dollar Amount in the briefcase.
        /// </returns>
        public int RetrieveBriefcaseAmountFromId(int id)
        {
            foreach (var briefcase in this.briefcases)
            {
                if (briefcase.Id == id)
                {
                    var value = briefcase.DollarAmount;
                    return value;
                }
            }

            return -1;
        }

        /// <summary>
        ///     Updates the round information.
        /// </summary>
        public void UpdateRoundInformation()
        {
            this.RoundManager.UpdateRound();
        }

        /// <summary>
        ///     Updates the cases left to open label.
        /// </summary>
        public void UpdateCasesLeftToOpenLabel()
        {
            this.RoundManager.CasesToOpenThisRound -= 1;
        }

        /// <summary>
        ///     Removes the specified briefcase from play.
        ///     Precondition: A briefcase is selected
        ///     Post condition: briefcase @id is removed.
        /// </summary>
        /// <param name="id">The id of the briefcase to remove from play.</param>
        /// <returns>
        ///     Dollar amount stored in the case, or -1 if case not found.
        /// </returns>
        public int RemoveBriefcaseFromPlay(int id)
        {
            var valueInCase = this.RetrieveBriefcaseAmountFromId(id);
            for (var briefcase = 0; briefcase < this.briefcases.Count; briefcase++)
            {
                if (this.briefcases.ElementAt(briefcase).Id == id)
                {
                    this.briefcases.RemoveAt(briefcase);
                    return valueInCase;
                }
            }

            return -1;
        }

        /// <summary>
        ///     Randomizes the briefcase amounts.
        /// </summary>
        public void RandomizeBriefcaseAmounts()
        {
            var rnd = new Random();
            var caseCounter = 0;

            while (this.randomAmounts.Count > 0)
            {
                var randomAmount = rnd.Next(this.randomAmounts.Count);
                var caseValue = this.randomAmounts.ElementAt(randomAmount);
                this.briefcases.Add(new Briefcase(caseCounter, caseValue));
                this.randomAmounts.RemoveAt(randomAmount);
                caseCounter++;
            }
        }

        private void checkGameVersion()
        {
            if (this.GameVersion == "Mega")
            {
                this.randomAmounts = this.Mega.ToList();
            }
            else if (this.GameVersion == "Syndicated")
            {
                this.randomAmounts = this.Syndicated.ToList();
            }
            else
            {
                this.randomAmounts = this.Regular.ToList();
            }
        }

        #endregion
    }
}