﻿using System;
using System.Collections.Generic;

namespace DealOrNoDeal.Model
{
    /// <summary>
    ///     Managers offers and banker results
    /// </summary>
    public class Banker
    {
        #region Data members

        private const int ExtendedRoundChecker = 13;

        private const int OneCaseLeftToOpen = 6;

        private const int OneCaseLeftToOpenExtendedRounds = 4;

        private readonly GameManager gameManager;

        #endregion

        #region Properties

        /// <summary>
        ///     Gets or sets the banker current offer.
        /// </summary>
        /// <value>
        ///     The banker current offer.
        /// </value>
        public double CurrentOffer { get; set; }

        /// <summary>
        ///     Gets or sets the banker minimum offer.
        /// </summary>
        /// <value>
        ///     The banker minimum offer.
        /// </value>
        public double MinOffer { get; set; }

        /// <summary>
        ///     Gets or sets the banker maximum offer.
        /// </summary>
        /// <value>
        ///     The banker maximum offer.
        /// </value>
        public double MaxOffer { get; set; }

        /// <summary>Gets or sets the banker average offer.</summary>
        /// <value>The banker average offer.</value>
        public double AverageOffer { get; set; }

        private double TotalOfferAmounts { get; set; }

        #endregion

        #region Constructors

        /// <summary>
        ///     Initializes a new instance of the <see cref="Banker" /> class.
        ///     Post condition: CurrentOffer == currentOffer; MinOffer == int.MaxValue; MaxOffer == int.MinValue;
        ///     gameManager == gameManager; AverageOffer == 0; TotalOfferAmounts == 0;
        /// </summary>
        /// <param name="currentOffer">The banker current offer.</param>
        /// <param name="gameManager">The game manager.</param>
        public Banker(double currentOffer, GameManager gameManager)
        {
            this.CurrentOffer = currentOffer;
            this.MinOffer = int.MaxValue;
            this.MaxOffer = int.MinValue;
            this.gameManager = gameManager;
            this.AverageOffer = 0;
            this.TotalOfferAmounts = 0;
        }

        #endregion

        #region Methods

        /// <summary>
        ///     Gets the offer.
        ///     Precondition: casesToOpenThisRound == 0;
        ///     Post condition: Sets the bankers current offer.
        /// </summary>
        /// <returns>
        ///     The bankers current offer.
        /// </returns>
        public double RetrieveOffer(ICollection<Briefcase> dollarAmountInPlay)
        {
            int casesToOpenNextRound;
            if (this.gameManager.CurrentRound < OneCaseLeftToOpenExtendedRounds &&
                this.gameManager.FinalRound == ExtendedRoundChecker)
            {
                casesToOpenNextRound = this.gameManager.CasesToOpenThisRound - 1;
            }
            else if (this.gameManager.CurrentRound < OneCaseLeftToOpen &&
                     this.gameManager.FinalRound != ExtendedRoundChecker)
            {
                casesToOpenNextRound = this.gameManager.CasesToOpenThisRound - 1;
            }
            else
            {
                casesToOpenNextRound = this.gameManager.CasesToOpenThisRound;
            }

            this.CurrentOffer = this.calculateOffer(dollarAmountInPlay, casesToOpenNextRound);
            this.updateMaxAndMinValues();
            this.TotalOfferAmounts += this.CurrentOffer;
            this.calculateAverageOffer();

            return this.CurrentOffer;
        }

        private int calculateOffer(IEnumerable<Briefcase> dollarAmountInPlay, int casesToOpenNextRound)
        {
            var remainingCases = 0;
            var dollarAmount = 0;
            foreach (var briefcase in dollarAmountInPlay)
            {
                remainingCases += 1;
                dollarAmount += briefcase.DollarAmount;
            }

            var bankerOffer = dollarAmount / casesToOpenNextRound / remainingCases;
            return (int) Math.Ceiling(bankerOffer / 100.0) * 100;
        }

        private void updateMaxAndMinValues()
        {
            if (this.gameManager.CurrentRound == 1)
            {
                this.MaxOffer = this.CurrentOffer;
                this.MinOffer = this.CurrentOffer;
            }
            else if (this.CurrentOffer < this.MinOffer || this.gameManager.CurrentRound == 1)
            {
                this.MinOffer = this.CurrentOffer;
            }
            else if (this.CurrentOffer > this.MaxOffer || this.gameManager.CurrentRound == 1)
            {
                this.MaxOffer = this.CurrentOffer;
            }
        }

        private void calculateAverageOffer()
        {
            var average = this.TotalOfferAmounts / this.gameManager.CurrentRound;
            this.AverageOffer = (int) Math.Ceiling(average / 100.0) * 100;
        }

        #endregion
    }
}