﻿namespace DealOrNoDeal.Model
{
    /// <summary>
    ///     Handles the information about each briefcase.
    /// </summary>
    public class Briefcase
    {
        #region Properties

        /// <summary>
        ///     Gets or sets the briefcase identifier.
        /// </summary>
        /// <value>
        ///     The briefcase identifier.
        /// </value>
        public int Id { get; set; }

        /// <summary>
        ///     Gets or sets the briefcase dollar amount.
        /// </summary>
        /// <value>
        ///     The briefcase dollar amount.
        /// </value>
        public int DollarAmount { get; set; }

        #endregion

        #region Constructors

        /// <summary>
        ///     Initializes a new instance of the <see cref="Briefcase" /> class.
        ///     Post condition: Id == id; DollarAmount == dollarAmount
        /// </summary>
        /// <param name="id">The briefcase identifier.</param>
        /// <param name="dollarAmount">The briefcase dollar amount.</param>
        public Briefcase(int id, int dollarAmount)
        {
            this.Id = id;
            this.DollarAmount = dollarAmount;
        }

        #endregion
    }
}