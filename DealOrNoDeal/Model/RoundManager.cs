﻿namespace DealOrNoDeal.Model
{
    /// <summary>
    ///     Manages the games rounds
    /// </summary>
    public class RoundManager
    {
        #region Data members

        private const int RegularRounds = 10;

        private const int ShortRounds = 7;

        private const int ExtendedRounds = 13;

        #endregion

        #region Properties

        /// <summary>
        ///     Gets or sets the current round.
        /// </summary>
        /// <value>
        ///     The current round.
        /// </value>
        public int CurrentRound { get; set; }

        /// <summary>
        ///     Gets or sets the cases to open.
        /// </summary>
        /// <value>
        ///     The cases to open.
        /// </value>
        public int CasesToOpenThisRound { get; set; }

        /// <summary>
        ///     Gets the final round.
        /// </summary>
        /// <value>
        ///     The final round.
        /// </value>
        public int FinalRound { get; }

        #endregion

        #region Constructors

        /// <summary>
        ///     Initializes a new instance of the <see cref="RoundManager" /> class.
        ///     Post condition: CurrentRound == currentRound; casesToOpenThisRound == casesToOpenThisRound;
        ///     FinalRound == (The number of rounds specified for that game)
        ///     Change this.FinalRound to equal RegularRounds; ExtendedRounds; or ShortRounds; in order to customize number of
        ///     rounds to play.
        /// </summary>
        /// <param name="currentRound">The current round.</param>
        /// <param name="casesToOpenThisRound">The cases to open this round.</param>
        public RoundManager(int currentRound, int casesToOpenThisRound)
        {
            this.CurrentRound = currentRound;
            this.CasesToOpenThisRound = casesToOpenThisRound;
            this.FinalRound = RegularRounds;
        }

        #endregion

        #region Methods

        /// <summary>
        ///     Gets the number of cases to open.
        /// </summary>
        /// <returns>
        ///     The number of cases to open for the round type you are playing.
        /// </returns>
        public int RetrieveNumberOfCasesToOpen()
        {
            if (this.FinalRound == ShortRounds)
            {
                return this.retrieveNumberOfCasesToOpenForShortRounds();
            }

            if (this.FinalRound == ExtendedRounds)
            {
                return this.retrieveNumberOfCasesToOpenForExtendedRound();
            }

            return this.retrieveNumberOfCasesToOpenForRegularRound();
        }

        private int retrieveNumberOfCasesToOpenForRegularRound()
        {
            int numberOfCasesToOpenNextRound;

            switch (this.CurrentRound)
            {
                case 1:
                    numberOfCasesToOpenNextRound = 6;
                    break;
                case 2:
                    numberOfCasesToOpenNextRound = 5;
                    break;
                case 3:
                    numberOfCasesToOpenNextRound = 4;
                    break;
                case 4:
                    numberOfCasesToOpenNextRound = 3;
                    break;
                case 5:
                    numberOfCasesToOpenNextRound = 2;
                    break;
                default:
                    numberOfCasesToOpenNextRound = 1;
                    break;
            }

            return numberOfCasesToOpenNextRound;
        }

        private int retrieveNumberOfCasesToOpenForShortRounds()
        {
            int numberOfCasesToOpenNextRound;

            switch (this.CurrentRound)
            {
                case 1:
                    numberOfCasesToOpenNextRound = 8;
                    break;
                case 2:
                    numberOfCasesToOpenNextRound = 6;
                    break;
                case 3:
                    numberOfCasesToOpenNextRound = 4;
                    break;
                case 4:
                    numberOfCasesToOpenNextRound = 3;
                    break;
                case 5:
                    numberOfCasesToOpenNextRound = 2;
                    break;
                default:
                    numberOfCasesToOpenNextRound = 1;
                    break;
            }

            return numberOfCasesToOpenNextRound;
        }

        private int retrieveNumberOfCasesToOpenForExtendedRound()
        {
            int numberOfCasesToOpenNextRound;
            switch (this.CurrentRound)
            {
                case 1:
                    numberOfCasesToOpenNextRound = 7;
                    break;
                case 2:
                    numberOfCasesToOpenNextRound = 5;
                    break;
                case 3:
                    numberOfCasesToOpenNextRound = 3;
                    break;
                default:
                    numberOfCasesToOpenNextRound = 1;
                    break;
            }

            return numberOfCasesToOpenNextRound;
        }

        /// <summary>
        ///     Moves to next round by incrementing Round property and setting
        ///     initial number of cases for that round
        ///     Precondition: None
        ///     Post condition: Round == Round@prev + 1 AND CasesRemainingInRound == (number of cases to open in the next round)
        /// </summary>
        public void UpdateRound()
        {
            this.CurrentRound += 1;
            this.CasesToOpenThisRound = this.RetrieveNumberOfCasesToOpen();
        }

        #endregion
    }
}